<?

/*

 Simple API example - 3rd party integration
 used when exchanging data from / connect to a schluss vault 

 WARNING: no security features built in yet!


/* Available endpoints

[GET] 	api.php?action=token
		Return a new session id (token)

[GET] 	api.php?action=poll&x=[sessionId]
		Get the current state

[GET] 	api.php?action=connecting&x=[sessionId]
		Update the state to 'connecting'

[POST] 	api.php?action=connected&x=[sessionId]
		@param: hash, Hash of where document is found (not used yet)
		@param: name, fieldname (for example 'email')
		@param: data, the value of the field (for example 'email@address.com')
		Update the state to 'connected'

[GET]	api.php?action=data&x=[sessionId]
		Return a JSON string containing the data thats stored
*/

switch ($_GET['action'])
{
	
	case 'token' :
		$sessionId = md5(microtime());
		updateState($sessionId, 'start');
		echo $sessionId;
	break;
	
	case 'poll' :
		echo getState($_GET['x']);
	break;
	
	case 'connecting' :
		updateState($_GET['x'], 'connecting');
		echo 'updated state: connecting';
	break;
	
	case 'connected' :
		if (isset($_POST))
		{
			// update state
			updateState($_GET['x'], 'connected');
			
			// save shared data
			saveShareData($_GET['x'], $_POST['hash'], $_POST['name'], $_POST['data']);
			echo 'saved share: connected';			
		}
	break;	
	
	case 'data' :
		header('Content-Type: application/json');
		echo getStored($_GET['x']);
	break;
	
	default : 	
		echo 'invalid input';
		exit();
}

function getStored($sessionId)
{
	$json = '{';
	
	if (file_exists(dirname(__FILE__) . '/store_' . $sessionId . '_email.txt'))
		$json .= '"email":"' . file_get_contents(dirname(__FILE__) . '/store_' . $sessionId . '_email.txt') . '",';

	if (file_exists(dirname(__FILE__) . '/store_' . $sessionId . '_firstname.txt'))
		$json .= '"firstname":"' . file_get_contents(dirname(__FILE__) . '/store_' . $sessionId . '_firstname.txt') . '",';

	if (file_exists(dirname(__FILE__) . '/store_' . $sessionId . '_lastname.txt'))
		$json .= '"lastname":"' . file_get_contents(dirname(__FILE__) . '/store_' . $sessionId . '_lastname.txt') . '",';
	
	$json = substr($json,0,-1);
	
	$json .= '}';

	return $json;
}

function saveShareData($sessionId, $hash, $name, $data)
{
	$filename = dirname(__FILE__) . '/store_' . $sessionId . '_' . $name . '.txt';
	$fp = fopen($filename, 'w+');
	fwrite($fp, $data);
	fclose($fp);
}

function updateState($sessionId, $state)
{
	$filename = dirname(__FILE__) . '/cache_' . $sessionId . '.txt';
	
	$fp = fopen($filename, 'w+');
	fwrite($fp, $state);
	fclose($fp);	
}

function getState($sessionId)
{
	$filename = dirname(__FILE__) . '/cache_' . $sessionId . '.txt';
	
	if (!file_exists($filename))
		return '';
	
	return file_get_contents($filename);
}



