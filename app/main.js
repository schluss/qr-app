//var $ = require("jquery");

// jquery:
window.$ = global.jQuery = require('jquery');
//require('bootstrap-pincode-input');
// jquery plugins:
require('jquery-pinlogin/dist/jquery.pinlogin.min.js');
require('jquery-toast-plugin');

//var $ = jQuery;

// crypto
global.CryptoJS = require('crypto-js');
global.cryptico = require('cryptico');


// storage
//global.localforage = require('localforage');
//global.IPFS = require('ipfs-mini');

// self created
global.tools = require('./scripts/tools.js');
global.context = require('./scripts/context.js');
global.entity = require('./scripts/entity.js');
global.provider = require('./scripts/provider.js');

// other
var app = require('sammy');

// manually include some sammy plugins, ugly, but the only way I know of that works
require('sammy/lib/plugins/sammy.template.js');
require('sammy/lib/plugins/sammy.nested_params.js');
require('sammy/lib/plugins/sammy.flash.js');
require('sammy/lib/plugins/sammy.title.js');

require('./app.js');
