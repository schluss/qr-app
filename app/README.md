
Install dev environment

Uses:
- Browserify to create single js bundje

1. Install NPM

2. Install these NPM modules:
npm install -g browserify
npm install -g watchify
npm install -g uglify-js
npm install -g gulp
npm install --save-dev gulp
npm install –save-dev gulp-uglify

In development:
Create an uncompressed bundle with source map, changes are automatic built with watchify

JS:
watchify main.js  --debug -o media/js/bundle.js -v

For in production:
Create compressed (uglify) bundje without source map:
browserify main.js  | uglifyjs -cm > media/js/bundle.js
 
browserify -t browserify-css main-css.js > media/css/bundle.css

idee: https://stackoverflow.com/questions/15590702/how-to-get-minified-output-with-browserify

