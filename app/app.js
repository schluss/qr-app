;(function($) {
	
	$.ajaxSetup({ cache: false }); // disable url caching

	// sammy: routing and displaying views
	var app = $.sammy('#main', function() {
	
		/// VARS -------------------
	
		this.persistentSession = true; 	// true: browser refresh does not delete session, false: login after browser refresh
	
		this.use('Template','tpl');
		this.use('Flash');
		this.use('Title');
		this.use('NestedParams');

		this.setTitle('Mijn kluis - Powered by Schluss');
		
		this.debug = true;
		
		//this.isLoaded = false;	// keeping track if app is loaded (first time loading)
		
		/// ROUTES -------------------
		
		this.get('#/requests', function(e){
			e.partial('views/requests.html', function(t){
				
				connectionShowrequests();
				
			});
		});
		
		this.get('#/', function(e) {
			
			e.partial('views/home.html', function(t){
				
				connectionShowrequests();
				
				$('#contextdata ul').append('<br/><h3>Mijn gegevens</h3>')
				
				// IPFS GET
				context.index.iterate(function(value, key, iterationNumber){
					
					
					context.getDoc(value.hash, function(doc) {
						
						var decrypted = CryptoJS.AES.decrypt(doc.content, entity.key).toString(CryptoJS.enc.Utf8);
						
						$('#contextdata ul').append('<li><a href="#/explore/'+key+'">' + decrypted + '</a></li>');
						
						
					});
					
				});
				
				/* LOCAL GET
				// get all data
				context.index.iterate(function(value, key, iterationNumber){
					
					context.getItem('docs', value.hash).then(function(doc){
						
						var decrypted = CryptoJS.AES.decrypt(doc.content, entity.key).toString(CryptoJS.enc.Utf8);
						
						$('#contextdata ul').append('<li><a href="#/explore/'+key+'">' + decrypted + '</a></li>');
						
					});
				});*/
				
			});
		});
		
		// GET: show view for adding a piece of data
		this.get('#/add', function(e){
			e.partial('views/add.tpl');			
		});
		
		// POST: Store a piece of data
		this.post('#/add', function(e){
			
					// encrypt data
			var encrypted = CryptoJS.AES.encrypt(e.params['content'], entity.key);
			

(async () => {
	try {

		let datahash = await context.setDoc({content : encrypted.toString()});
		
		console.log('test' + datahash);
		let result = await context.setItem('index', e.params['name'], { 
					name : e.params['name'], 
					hash : datahash, 
					app : 'self', 
					type : 'textarea' ,
					timestamp: Math.round(new Date().getTime()/1000), 
					version : '0.1' });
					
		
		e.redirect('#/');
		
	}  catch (a) {
		console.log(a);
	}
})();	
			/*
			// store data (first document in ipfs, than add to index)
			context.setDoc({content : encrypted.toString()},function(datahash){

				context.setItem('index', e.params['name'], { 
					name : e.params['name'], 
					hash : datahash, 
					app : 'self', 
					type : 'textarea' ,
					timestamp: Math.round(new Date().getTime()/1000), 
					version : '0.1' })
					.then(function(result){
						var t = e;
						t.redirect('#/');
				});
			});*/
		});		
		
		// GET: show the contents of a piece of stored data
		this.get('#/explore/:key', function(e) {
			
			context.getItem('index', e.params['key']).then(function(index){
				
				// IPFS STORAGE
				context.getDoc(index.hash, function(doc) {
					
					var decrypted = CryptoJS.AES.decrypt(doc.content, entity.key).toString(CryptoJS.enc.Utf8);	
										
					e.partial('views/explore.tpl', { index : index, doc : doc, data : decrypted}, function(t){
						
						var frm = '<input type="'+index.type+'" name="" value="'+decrypted+'" />';	
						$('#formcontent').append(frm);	
					
					});
				});
				
				/*
				// LOCAL STORAGE
				context.getItem('docs', index.hash).then(function(doc){
					
					var decrypted = CryptoJS.AES.decrypt(doc.content, entity.key).toString(CryptoJS.enc.Utf8);	
					
					e.partial('views/explore.tpl', { index : index, doc : doc, data : decrypted}, function(t){
						
						var frm = '<input type="'+index.type+'" name="" value="'+decrypted+'" />';	
						$('#formcontent').append(frm);	
					
					});
				});
				*/
			})
		});		

		this.get('#/register', function(e) {
		  e.partial('views/register.html');
		});
		
		this.post('#/register', function(e){
			
			// register user
			entity.register(e.params['pin'], function(){
				
				// autologin user
				entity.login(e.params['pin'], function(result){
				
					// and where done, so redirect.
					e.redirect('#/');
				});
			});
		});		

		this.get('#/login', function(e) {
			this.title('Inloggen');
			e.partial('views/login.html');
		});	
		
		this.post('#/login', function(e){
			
			entity.login(e.params['pin'], function(result){
				
				if (result)
				{					
					$.toast({ 
						heading 	: 'Pincode correct',
						text 		: 'Je kluis wordt geopend',
						position	: 'top-center',
						loader 		: false,
						icon		: 'success'
					});					
					e.redirect('#/');
				}
				else
				{
					$.toast({ 
						heading 	: 'Inloggen mislukt',
						text 		: 'Ongeldige pincode, probeer het opnieuw',
						position	: 'top-center',
						loader 		: false,
						icon		: 'error'
					});					
					// todo, make nice flash message
					//alert ('Ongeldige pincode, probeer het opnieuw');
				}
				
			});			
		});
		
		this.get('#/logout', function(e) {
			
			// logout user
			entity.logout(function(){
				
				// run unload tasks
				e.trigger('app-onunloaded');
				
				// redirect back
				e.redirect('#/');
			});
		});	

		this.get('#/settings', function(e) {
			
			e.partial('views/settings.html');
		});		
		
		this.get('#/connections', function(e) {
			e.title('Deeloverzicht');
			e.partial('views/connections.html');
		});	

		this.get('#/request/:appname', function(e) {
			
			context.getItem('requests', e.params['appname']).then(function(requestInfo){
			
				// set the request
				provider.request = requestInfo;
			
				provider.init(requestInfo.settingsuri, function(result){
			
					e.partial('views/request.tpl', { provider : provider.settings}, function(t){
						
						provider.renderScopeForm(function(frm){
							
							$('#formcontent').append(frm);
							
						});
						
					});
				
				});
			
			});
		});	

		this.post('#/request/:appname', function(e){
			provider.processRequest(e.params, function(result){
								
				// todo show nice success message and redirect
				//alert ('Jouw data is opgeslagen en als je aangegeven hebt dat de gegevens wilde delen, dan is dat nu gedaan');
				e.redirect('#/request_done/' + e.params['appname']);
			});		
		});	

		this.get('#/request_done/:appname', function(e){
			
			// delete open request
			context.removeItem('requests',e.params['appname']);
			
			e.title('Bestelling geplaatst');
			e.partial('views/request_done.html');
		});
		
		
		// handle, initialize and redirect 3rd party connection requests 
		this.get('#/connect/:token/:settingsuri', function(e) {
					
			context.init(function(){		
			
				$.getJSON(e.params['settingsuri'], function(result){
					
					// save it in db	
					context.setItem('requests', result.appname, { token : e.params['token'], name : result.name, settingsuri : e.params['settingsuri']});
					//context.storeConnectionRequest(result.appname, data['token'], result.name, data['settingsuri']);
				
					// update client api to 'connecting'
					app.trigger('connection-update-clientapi', { token :  e.params['token'], apiuri : result.apiUri, action : 'connecting'});
						
					// redirect 'home' -> and automatic go to registration or login when needed
					e.redirect('#/');						
				});			
			});	
		});		
		
		// Before every request, do some checks!
		this.around(function(callback){
			
			var e = this;
			
			// always, exept login|register|connect
			// note: connect url does not match with contextMatchesOptions, so used a hack in the if...
			if (app.contextMatchesOptions(this, {except: {path: ['#/login', '#/register', '#/connect']}}) && this.path.indexOf('#/connect/') === -1)
			{
				//console.log ('around triggered: always, exept login|register|connect');
				context.init(function(){
				
					// check if logged in
					if (!entity.authenticated)
					{
						if (app.persistentSession)
						{
							context.getItem('db', 'session').then(function(pincodeHash){ 
								
								if (pincodeHash != null)
								{
									entity.authenticate(pincodeHash, function(){
										e.trigger('app-onloaded');
										callback();
									});
								}
								else
								{
									e.redirect('#/login');
									//return false;
								}
								
							});
						}
						else
						{
							e.redirect('#/login');
							//return false;
						}
					}
					
					// logged in:
					else
					{				
						e.trigger('app-onloaded');
						callback();
					}
					
				});				
			}

			// only: register
			else if (app.contextMatchesOptions(this, '#/register'))
			{
				//console.log ('around triggered: register');
				context.init(function(){			
				
					console.log ('e created: ' + entity.created);
					// when already registered
					if (entity.created)
					{
						e.redirect('#/');
					}
					else
					{
						callback();
					}
				});				
			}
		
			// only: login
			else if (app.contextMatchesOptions(this, '#/login'))
			{
				//console.log ('around triggered: login');
				context.init(function(){
					
					// when already loggedin
					if (entity.authenticated)
					{
						e.redirect('#/');
						//return false;
					}
					
					// when not registered
					else if (!entity.created)
					{
						e.redirect('#/register');
						//return false;
					}
					
					// show login
					else
					{
						callback();
					}
				});				
			}
			else
				callback();
			
		});
		
		/* OLD before triggers...
		// run always before handling action, except on registration & login (&connect/*)
		this.before({except: /\#\/(login|register|connect\/.*)$/}, function(e){
			
			context.init(function(){
			
				// check if logged in
				if (!entity.authenticated)
				{
					if (app.persistentSession)
					{
						context.getItem('session').then(function(pincodeHash){
							
							if (pincodeHash != null)
							{
								entity.authenticate(pincodeHash, function(){
									return true;
								});
							}
							else
							{
								e.redirect('#/login');
								return false;
							}
							
						});
					}
					else
					{
						e.redirect('#/login');
						return false;
					}
				}
				
				e.trigger('app-onloaded');
			});
		});
		
		// run only at registration page
		this.before({only: /\#\/register$/}, function(e){

			context.init(function(){			
			
				console.log ('e created: ' + entity.created);
				// when already registered
				if (entity.created)
				{
					e.redirect('#/');
					return false;
				}
			});
		});
		
		// run only at login page
		this.before({only: /\#\/login$/}, function(e){

			context.init(function(){
				
				// when already loggedin
				if (entity.authenticated)
				{
					e.redirect('#/');
					return false;
				}
				
				// when not registered
				if (!entity.created)
				{
					e.redirect('#/register');
					return false;
				}
			});
		});
		*/
		/// EVENTS -------------------
		
		// when the app is loaded, disply extra screens and do extra checks etc
		this.bind('app-onloaded', function(e, data){
			
			$('header').show();
			
			//app.trigger('connection-show-requests');
		});
		
		// when user logged out : app unloaded, hide all extra's
		this.bind('app-onunloaded' , function(e, data){
			
			$('header').hide();
		});
		
		// Update api of third party connection request by sending a request to apiuri
		// call: trigger('connection-update-clientapi', { token : '', apiuri : '', action : 'connecting|connected'});
		this.bind('connection-update-clientapi', function(e, data){
			
			// todo: if appname is given (and not token+apiuri), get the needed details from requests table in storage
			
			$.get(data['apiuri'] + '?x='+encodeURIComponent(data['token'])+'&action=' + data['action']);
		});
		
		function connectionShowrequests()
		{
			var html = '';
			context.getItemCount('requests').then(function(count){
				
				//$('#requests').html('<h3>Openstaande deelverzoeken:</h3>');
				
				if (count == 0 || count == null)
				{
					$('.bellnotify').hide();
					
					$('#displayRequests').hide();
					
					//$('#requests').append('<p>Geen openstaande inzageverzoeken</p>');
					
					return;
				}
				
				$('.bellnotify').show();
				$('#displayRequests').show();
				
				$('#requests').append('<h3>Openstaande inzageverzoeken:</h3>');

				context.getItems('requests',function(value, key, iterationNumber){	
			
					$('#requests').append('<div><a href="#/request/'+key+'">' + value.name + '<a/></div>');
				});
				
				
			});
		}
		
		/*
		this.bind('connection-show-requests', function(e, data){
			
			console.log ('triggered: connection-show-requests');
		});
		*/
		
		$(function() {
			app.run('#/');
		});
	});
})(jQuery);