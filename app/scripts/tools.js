var Tools = {
		// Generate a 'random' hash with given length
		hash : function(length) {
		  var txt = "";
		  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		  for (var i = 0; i < length; i++)
			txt += possible.charAt(Math.floor(Math.random() * possible.length));

		  return txt;
		},
		// generate semi unique (g)uuid in javascript
		// source: https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
		uuidv4 : function() {
		  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		  });
		}
	};
	
module.exports = Tools;