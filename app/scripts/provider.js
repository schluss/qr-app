	// connected 3rd party
	var provider = {
		
		request : null, 	// holds the request, if there's one
		settings : null,	// holds the contents of the [provider].settings.json
		scopedata : [],		// holds all the scope fields and data
		
		init : function(settingsuri, callback){
			
			console.log('init provider: ' + settingsuri);
			
			// get json
			$.getJSON(settingsuri, function(result){

				provider.settings = result;
				
				// get existing scope data
				provider.getScopeData(function(){
					
					
					// when done, go on
					callback();
					
				});
			});			
		},
				
		// get data thats already in storage (if there is)
		getScopeData : function(callback){
			
			if (provider.settings == '' || provider.settings.scope.length == 0)
			{
				return callback(false);	
			}
			
			var count = provider.settings.scope.length;
			
			// traverse scope:
			for (var i = 0; i < provider.settings.scope.length; i++)
			{
				provider.scopedata[provider.settings.scope[i].name] = provider.settings.scope[i];
				
				
				context.getItem('index', provider.settings.scope[i].name).then(function(index){
					
					if (index == null)
					{
							count--;
							
							if (count === 0)
								callback(true);
					}
					else
					{
						// IPFS GET
						context.getDoc(index.hash, function(doc){
							
							var decrypted = CryptoJS.AES.decrypt(doc.content, entity.key).toString(CryptoJS.enc.Utf8);	

							provider.scopedata[index.name].value = decrypted;
							provider.scopedata[index.name].hash = index.hash;

							count--;
							
							if (count === 0)
								callback(true);
						});
						
						/*
						// LOCAL GET
						context.getItem('docs', index.hash).then(function(doc){
							
							var decrypted = CryptoJS.AES.decrypt(doc.content, entity.key).toString(CryptoJS.enc.Utf8);	

							//provider.scopedata[index.name] = { value : decrypted, hash : index.hash };
							provider.scopedata[index.name].value = decrypted;
							provider.scopedata[index.name].hash = index.hash;

							count--;
							
							if (count === 0)
								callback(true);
							
						});*/
					}
					
					
				});
			}
			
			
		},
		
		// render the form
		renderScopeForm : function(callback)
		{
			var form = $('<form/>', {
				action : '#/request/' + provider.settings.appname,
				method : 'post'
			});
			
			
			//for (var i = 0; i < provider.scopedata.length; i++)
			for (name in provider.scopedata)
			{
				
				var value = provider.scopedata[name].value == undefined ? '' : provider.scopedata[name].value;
				
				var html = 	'<p style="padding:10px;background:#fff;border-radius:5px;">' +
							'<label for="'+name+'" style="font-weight:bold;font-size:1.2em;color:#fb637e;">'+provider.scopedata[name].label+'</label><br/>';
				
				// switch between different field types
				switch (provider.scopedata[name].type)
				{
					case 'textarea' : 
						html += '<textarea name="val['+name+'][value]" id="'+name+'">'+value+'</textarea>';
					break;
					default:
						html += '<input type="'+provider.scopedata[name].type+'" name="val['+name+'][value]" id="'+name+'" value="'+value+'"/> ';
				}
					
				if (!provider.scopedata[name].required)	
				{
				html += '<br/><span style="color:#888">'+provider.scopedata[name].description+'</span><br/><br/>' +
						'Delen met ' + provider.settings.name +'?<br/>' +
						'<input type="checkbox" name="val['+name+'][share]" value="true" id="share_'+name+'" /></p><br/><br/>'
				}
				else
				{
					// send as hidden field
					html += '<input type="hidden" name="val['+name+'][share]" value="true" id="share_'+name+'" />';
				}
				
				//console.log('item:');
				//console.log (provider.scopedata[name]);
				
				// sub items (options)
				//if (provider.scopedata[name].options != ''){
					
					for (o in provider.scopedata[name].options)
					{
						var opt = provider.scopedata[name].options[o];
						
						//console.log(opt);
						
						html += '<p>';
						
						// switch between different field types
						switch (opt.type)
						{
							default:
								html += '&nbsp;&nbsp;<input type="checkbox" name="val['+opt.name+'][value]" id="'+opt.name+'" value=""/> ';
						}
						
						html += '<label for="val['+opt.name+'][value]" style="font-weight:bold;">'+opt.label+'</label><br/>';
						
						html += '</p>';
					}
					
					
				//}
				
				
				form.append(html);
			}			
			
			var savelabel = "Opslaan";
			
			if (provider.settings.savelabel != '')
				savelabel = provider.settings.savelabel;
			
			form.append('<p style="text-align:center;"><br/><button type="submit">'+savelabel+'</button></p>');

			// done, send form back
			callback(form);
		},
		
		processRequest : function(formParams, callback){
			
			// per item:
			var count = Object.keys(formParams.val).length;

			Object.keys(formParams.val).forEach(function(key) {

				setTimeout(function() {

					// IPFS STORE

					// prepare data 
					var encrypted = CryptoJS.AES.encrypt(formParams.val[key].value, entity.key);
						
					// store data
					context.setDoc({content : encrypted.toString()}, function(datahash)
					{
						// store in index
						context.setItem('index', key, { name : key, hash : datahash, app : provider.settings.appname, type : provider.scopedata[key].type ,timestamp: Math.round(new Date().getTime()/1000), version : '0.1' }).then(function(result){
							
							if ('share' in formParams.val[key])
							{
								console.log (key + ' is shared with 3rd party, so send it');
								
								// send post to provider api to share the data
								provider.shareDataToProvider(datahash, key, formParams.val[key].value);
							}
							
								count -= 1;
								if (count === 0)
								{
									console.log('every field processed');
									// call your callback here
									//callback();
								}
						});
					});
	
/*				
// LOCAL STORE
						// generate new / load existing hash
						var datahash;

						if (provider.scopedata[key].hash != undefined)
							datahash = provider.scopedata[key].hash;
						else
							datahash = tools.uuidv4();
					
						// store in index
						context.setItem('index', key, { name : key, hash : datahash, app : provider.settings.appname, type : provider.scopedata[key].type ,timestamp: Math.round(new Date().getTime()/1000), version : '0.1' }).then(function(result){
							
							// prepare data 
							var encrypted = CryptoJS.AES.encrypt(formParams.val[key].value, entity.key);
							
							// store data
							context.setItem('docs', datahash, {content : encrypted.toString()}).then(function(result){});				
					
							if ('share' in formParams.val[key])
							{
								console.log (key + ' is shared with 3rd party, so send it');
								
								// send post to provider api to share the data
								provider.shareDataToProvider(datahash, key, formParams.val[key].value);
							}					
					
						});
					
					count -= 1;
					if (count === 0)
					{
						console.log('every field processed');
						// call your callback here
						callback();
					}
					*/	
						
				}, 0);
			});
			
			callback();
		},
		
		// internal use to async save a doc
		shareDataToProvider : function(hash, name, value){
			$.post(provider.settings.apiUri + '/?x=' + provider.request.token + '&action=connected', { hash : hash, name : name, data : value}, function(data){});
		}
	};
	
module.exports = provider;