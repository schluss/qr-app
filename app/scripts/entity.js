var settings = require('./config.js');

	// object to keep track of the 'entity' (user) and session
	// after one single page refresh, all of this is gone and user needs to login again!
	// future: maybe allow storage of these in a cookie, to allow persistent sessions, for now this is a deliberate choice for improved security!
	var entity = {
		
		created : null,		// is the database created and present
		authenticated : false,	// is the entity logged in
		pincodehash : '',		// for this session: the hash that decrypts the key
		key : '',				// contains the key while in a valid session
		
		register : function(pin, callback){
			
			//showpopup('Sleutels genereren... Dit kan even duren');
			
			//console.log('registreren');
			
			// hash pin
			var pincodeHash = CryptoJS.SHA256(pin);
			
			// generate unique and random key
			var key = tools.hash(64); // 256bits hash
			
			// encrypt key with pincodeHash
			var encrypted = CryptoJS.AES.encrypt(key, pincodeHash.toString());
			
			// create RSA key pair
			var RSAPrivateKey = cryptico.generateRSAKey(key, 1024); // 2048 preferred, but takes a lot of time (so this quicker testing, change in production version!)
			
			// create a new 'app' instance	
			context.setItem('db','app', {version : '1.0', name : 'Schluss'})
				.then(function(){
					
					// store key
					return context.setItem('db','key', encrypted.toString());
				})
				.then(function(){
					
					// store rsa private key
					return context.setItem('db','rsakey', RSAPrivateKey);
					
				})
				.then(function(){
					
					// create a session right away
					entity.created = true;
					
					//hidepopup();
					
					// all done, call back!
					callback();
				});
			
		},
		login : function(pin, callback){
			
			// hash pin
			var pincodeHash = CryptoJS.SHA256(pin);
			
			entity.authenticate(pincodeHash.toString(), function(result){callback(result)});
		},
		// when persistentSession, authenticate using pincodehash
		authenticate : function(pincodeHash, callback){
			
			context.getItem('db','key').then(function(key){
				
				try
				{
					var decryptedkey = CryptoJS.AES.decrypt(key, pincodeHash);
					
					if (decryptedkey == '')
						throw('invalid key');
					
					// try to decode - todo, create proper pincode hash comparison instead of 'just try to decrypt and error on fail'
					var k = decryptedkey.toString(CryptoJS.enc.Utf8);
					
					// all went well, save user in 'session'
					entity.authenticated = true;
					entity.pincodehash = pincodeHash;
					entity.key = k;
					
					// when there's a persistent session store pincode hash in context
					if (settings().persistentSession)
					{
						context.setItem('db','session', pincodeHash).then(function(){
							callback(true);
						});
					}
					else
						callback(true);
				}
				catch(e)
				{
					console.log(e);
					callback(false);
				}				
				
			});			
		},
		logout : function(callback){
			
			entity.authenticated = false;
			entity.pincodehash = '';
			entity.key = '';

			// remove persistentSession
			if (settings().persistentSession)
			{
				context.removeItem('db','session').then(function(){
					callback();
				});
			}
			else
				callback();
		}
	};	
	
module.exports = entity;