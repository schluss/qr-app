var localforage = require('localforage');
var IPFS = require('ipfs-mini');
var settings = require('./config.js');

	var context = {
		
		appversion : '0.1',
		keyversion : '0.1',		
		
		db : null,			// app specific and local storage: keys: app,key
		requests : null,	// holds 3rd party connection requests
		connections : null,	// holds 3rd party connections
		shares : null,		// holds shares
		index : null,		// holds indexes
		docs : null,		// holds data (later on ipfs)
		ipfs : null,
		
		_isOpen : false,
		
		init : function(callback){
		
			// when open already , stop processing
			if (this._isOpen)
				return callback();
			
			// create / open / init indexeddb instances
			this.db = localforage.createInstance({
				name 		: 'schluss',
				version     : 1.0,
				storeName   : 'vault',
				description : 'schluss data storage'
			});
			
			this.requests = localforage.createInstance({
				name 		: 'requests',
				version     : 1.0,
				storeName   : 'requests',
				description : 'schluss requests storage'
			});
			
			this.connections = localforage.createInstance({
				name 		: 'connections',
				version     : 1.0,
				storeName   : 'connections',
				description : 'schluss connections storage'
			});

			this.shares = localforage.createInstance({
				name 		: 'shares',
				version     : 1.0,
				storeName   : 'shares',
				description : 'schluss shares storage'
			});
			
			this.index = localforage.createInstance({
				name 		: 'index',
				version     : 1.0,
				storeName   : 'index',
				description : 'schluss index storage'
			});			
			
			this.ipfs = new IPFS({ host: settings().IPFSHost, port: settings().IPFSPort, protocol: settings().IPFSProtocol });
			//this.ipfs = new IPFS({ host: 'ipfs.infura.io', port: 5001, protocol: 'https' });
			
			this.docs = localforage.createInstance({
				name 		: 'docs',
				version     : 1.0,
				storeName   : 'docs',
				description : 'schluss file store'
			});			
		
			this.db.ready().then(function() {

				// check if database is present
				context.getItem('db', 'app').then(function(val){
					
					if (val != null)
					{
						// if so, store the state in the enitity
						entity.created = true;
						this._isOpen = true;
					}
					
					callback();
				});
				
			}).catch(function (e) {
				console.log(e); // `No available storage method found.`
				// One of the cases that `ready()` rejects,
				// is when no usable storage driver is found
			});

		},
		
		// Generic db access methods
		
		getItem : function(table, key)
		{
			return this[table].getItem(key);
		},
		setItem : function(table, key, value)
		{
			return this[table].setItem(key,value);
		},		
		removeItem : function(table,key){
			return this[table].removeItem(key);
		},	
		//getItemCount : function(table,callback){
		getItemCount : function(table){
			return this[table].length();
			//return this[table].length(function(numberOfKeys){callback(numberOfKeys);});
		},	
		getItems : function(table, callback){
			return this[table].iterate(function(value,key,iterationNumber){callback(value,key,iterationNumber);});
		},
		
		// ipfs methods
		setDoc : function(contentJSON, callback)
		{
			this.ipfs.addJSON(contentJSON, function(err, result) {
			  //console.log(err, 'store IPFS: ' + result);
			  
			  if (err == null)
				callback(result);
			  //return (result);
			});
		},
		
		getDoc :function(hash, callback)
		{
			try{
				this.ipfs.catJSON(hash, function(err, result) {
					
				  //console.log(err, 'get IPFS: ' + result);
				  
				  //this.statDoc(hash);
				  
				  if (err == null)
					callback(result);
				});
			}
			catch(err)
			{
				console.log (err);
			}
		},
		
		
		// show ipfs stats of a stored doc
		statDoc : function(hash)
		{
			this.ipfs.stat(hash, function(err, result) {
			  console.log(err, result);
			});	
		}
	};
	
module.exports = context;