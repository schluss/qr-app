module.exports = function() {
	return {
		persistentSession : true,
		//IPFSHost : "ipfs.schluss.app"
		IPFSHost : "ipfs.infura.io",
		IPFSPort : 5001,
		IPFSProtocol : 'https'
	};
}