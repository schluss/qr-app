<?php

// get a new token from the Schluss API
$token = file_get_contents('https://www.schluss.org/qr/api?action=token');

// generate redirect url to open the app and starting a new connection request
$url = 'https://www.schluss.org/qr/app/#/connect/'.$token.'/https%3A%2F%2Fwww.schluss.org%2Fqr%2Fschluss.settings.json';

header('location:' . $url);